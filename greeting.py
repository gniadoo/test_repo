
class GreetingGenerator:

    @staticmethod
    def hello(name):
        return "Hello {}!".format(name)

    @staticmethod
    def hi(name):
        return "Hi {}!".format(name)

    @staticmethod
    def group_hello(name1, name2):
        return "Hello {} and {}!".format(name1, name2)

    @staticmethod
    def group_hi(name1, name2):
        return "Hi {} and {}!".format(name1, name2)

    @staticmethod
    def group_whats_up(name1, name2):
        return "Whats up {} and {}!".format(name1, name2)
